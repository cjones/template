""" Simple setup to define the entry point for the main module"""

import setuptools

setuptools.setup(
    name='template',
    version='1.0.0',
    install_requires=[],
    packages=setuptools.find_packages(),
    entry_points='''
        [console_scripts]
        template=template.shell:cli
    ''',
)
