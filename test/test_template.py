#!/usr/bin/python3
# vim: tabstop=4 shiftwidth=4 softtabstop=4
"""
test_template.py

Tests that the underlaying calls are made.

Usage:
> tox -epep8 (for PEP8/flake8 tests)
> tox -epy3
or
> tox -e
"""
import pathlib
import pytest
import mock

from template import template


class TestTemplate():
    '''This class tests the Template class'''

    def test_bad_dir(self):
        '''Test where a directory doesn't exist'''
        os_agnostic_path = str(pathlib.PurePath('./foo_dir/'))
        with pytest.raises(FileNotFoundError):
            template.Template(os_agnostic_path)

    @mock.patch("pathlib.Path.exists", return_value=True)
    def test_path_exists(self, pathlib_patch):
        '''Test where both remotes do no exist'''
        os_agnostic_path = str(pathlib.PurePath('/somepath'))
        template.Template(os_agnostic_path)
        assert pathlib_patch.call_count == 1
