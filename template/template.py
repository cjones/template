#!/usr/bin/python3
# vim: tabstop=4 shiftwidth=4 softtabstop=4

"""
Template main class
"""
import argparse
import pathlib
import sys


TEMPLATE_VERSION = '1.0.0'


class TemplateArgParse():
    """Does the argument parsing for this class"""

    def __init__(self):
        self._args = self._parse_args()
        self._version = TEMPLATE_VERSION

    def get_version(self):
        """Gets the version of this package"""
        return self._args.random_dir

    def get_random_dir(self):
        """Gets the value of the random dir"""
        return self._args.random_dir

    def _parse_args(self):
        description = 'Template of arg parse'
        parser = argparse.ArgumentParser(description=description)
        help_text_random_dir = 'Random directory for example'
        parser.add_argument('random_dir', help=help_text_random_dir)
        if len(sys.argv) < 2:
            parser.print_help(sys.stderr)
            sys.exit(1)
        return parser.parse_args()


class Template():
    """Initializes this class and does the work it is supposed to do"""

    def __init__(self,
                 file_path_string):
        print('\n\n*Initialization of Template Class*\n')
        self._file_path = pathlib.Path(file_path_string)
        print('\n*Initialization of Template Class Complete*\n')
        self._verify_args()

    def _verify_args(self):
        if not self._file_path.exists():
            raise FileNotFoundError

    def do_work(self):
        """Do the necessary work in this class"""
        print('\n\n*Template Class is doing work*\n')
        print('HERE IS MY WORK')
        print('\n*Template Class is done doing work*\n')

    def print_results(self):
        """Print the necessary results for this class"""
        print('\n\n*Template Class is printing results*\n')
        print(f'This is the current file_path: ${self._file_path}')
        print('\n*Template Class is done printing results*\n')


def main():
    """Main f'n"""
    arg_parser = TemplateArgParse()
    template_instance = Template(arg_parser.get_random_dir())
    template_instance.do_work()
    template_instance.print_results()


if __name__ == "__main__":
    main()
