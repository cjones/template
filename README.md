# template

This is a template program to demonstrate a simple python module setup

Basic arg parsers are in place
Basic setup.py is in place
Basic linting is setup

Clone this repo and adjust as necessary to suit your needs

Execute with the following commands:

```
python3 -m venv ./.venv
source ./.venv/bin/activate
python -m pip install -r requirements.txt
python template/template.py <empty local folder to demonstrate example>
```

Example:
```
mkdir foobar
python template/template.py ./foobar
```

## Basis
- This is based on several templates created over the years

## Features
- Currently only ssh is supported.

### General
- `python template/template.py <empty local folder to demonstrate example>`

### Self testing
- `tox` - run both flake8, plylint, and py3 pytest tests. This also installs all dependencies in .tox in a virtualenv
- `tox -e flake8` - for pep8/flake8 linting of the python files.
- `tox -e pylint` - for more detailed linting of the python files.
- `tox -e py3` - run unit tests against fake data to ensure things work as desired.

*or*

- `python3 -m venv ./.venv`
- `source ./.venv/bin/activate`
- `python -m pip install -r test-requirements.txt`

*then*

- `python -m pytest -v -s` - verbose and stdout

*or*

- `python -m pytest test/test_template.py::TestTemplate::test_bad` - specific test

## Requirements

You will need to have python3 installed on your computer
-  [Python3 Installation](https://www.python.org/downloads/)

## Known Issues

None at this time

## Release Notes

- First release

## Credits

Thanks to the following who have helped contributed in alpha order:
- `@alex` - You know who you are!
- `@ivan` - Thanks for getting my testing brain back in order.

### 1.0.0

Initial release of template

-----------------------------------------------------------------------------------------------------------

### For more information

* [Python3 Installation](https://www.python.org/downloads/)

**Enjoy!**
